import React, { Component } from 'react';
import './assets/css/default.min.css'
import {
  BrowserRouter as Router,
  Route
} from 'react-router-dom';
import Header from './components/header_components/header'
import Homepage from './components/pages/homepage'
import TweeterStats from './components/pages/twitter_stats'
import StockOptions from './components/pages/stock_options'
import Results from './components/pages/results'

class App extends Component {
  render() {
    return (
        <Router>
        <div className="App">
          <Header />
            <Route exact path="/" component={Homepage} />
            <Route exact path="/twitter-stats" component={TweeterStats} />
            <Route exact path="/stock-options" component={StockOptions} />
            <Route exact path="/results" component={Results} />
        </div>
      </Router>

    );
  }

}

export default App;
