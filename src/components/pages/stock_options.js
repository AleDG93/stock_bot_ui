import React, { Component } from 'react';
import stockData from '../../stock_options.txt';
import ScrollArea from 'react-scrollbar';


class StockOptions extends Component {
    
       
    constructor(props){
        super(props);
        this.state = {
          data: []
        }
      }
    
    getData(){

        var stocks;
        var stockDivs = [];
        var jsonStocks = {
            "stocks": []
        };
        fetch(stockData)
        .then((r) => r.text())
        .then(text  => {
            stocks = text;
        })
        .then(() => {
            stockDivs = stocks.split("{    \"Meta Data\"");
            }
        ).then(() => {
            stockDivs.forEach(element => {
                if(element.trim() !== ""){
                    var myElement = "{    \"Meta Data\"" + element;
                    var jsonifyStock = JSON.parse(myElement);
                    jsonStocks.stocks.push(jsonifyStock);
                }
            });
        }).then(() => {
            console.log(jsonStocks.stocks)
            this.setState({
                data: jsonStocks.stocks
            })
        })
      }
    
    componentDidMount(){
        this.getData();
    };

    render() {

    return (
        <div> <div className="container border-tb">
        <div className="full-c-third one-r border-tb">
            <h3 className="subtitle">Some statistics</h3>
        </div>
        <div className="f-c-third m-l">
            <h4>Total number of records:</h4>
            <p>{this.state.data.length}</p>
        </div>
       
    </div>  
        <ScrollArea
        speed={0.8}
        className="area"
        contentClassName="content"
        horizontal={false}
        >   
      {
        this.state.data.reverse().map((item, i) => (
            <div className="table-div m-b-l m-t-l">
        <ScrollArea
        speed={0.8}
        className="area"
        contentClassName="content"
        horizontal={false}
        > 
            <div key={i} className="container row ">
                <div className="full-c-third m-l m-s">
                <table>
                    <tr>
                        <th>
                        Infromation: {item['Meta Data']['1. Information']}
                        </th>    
                        <th>
                            Symbol: {item['Meta Data']['2. Symbol']}                    
                        </th>
                        <th>
                            Last Refreshed: {item['Meta Data']['3. Last Refreshed']}                    
                        </th>
                        <th>
                            Interval: {item['Meta Data']['4. Interval']}                    
                        </th>
                        <th>
                            Output Size: {item['Meta Data']['5. Output Size']}    
                        </th>
                        <th>
                            Time Zone: {item['Meta Data']['6. Time Zone']}
                        </th>
                    </tr>
                    </table>
                    </div>
                    <div className="full-c-third m-l m-s">



                    {Object.keys(item['Time Series (1min)']).map((el, e) => (
                                     <table>

                        <tr>
                            <th>
                                Timestamp
                            </th>
                            <th>
                                1. open
                            </th>
                            <th>
                                2. high
                            </th>
                            <th>
                            3. low
                            </th>
                            <th>
                            4. close
                            </th>
                            <th>
                            5. volume
                            </th>
                        </tr>
                        <tr>
                            <td>
                                {el}
                            </td>
                            <td>
                                {item['Time Series (1min)'][el]['1. open']}
                            </td>
                            <td>
                                {item['Time Series (1min)'][el]['2. high']}
                            </td>
                            <td>
                                {item['Time Series (1min)'][el]['3. low']}
                            </td>
                            <td>
                                {item['Time Series (1min)'][el]['4. close']}
                            </td>
                            <td>
                                {item['Time Series (1min)'][el]['5. volume']}
                            </td>
                        </tr>

                </table>

                    ))}
                </div>  
        </div></ScrollArea></div>
        ))
        }

      </ScrollArea></div>
    );
    }
}

export default StockOptions;
