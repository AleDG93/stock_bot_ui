import React, { Component } from 'react';

const centerText = {
    textAlign: 'center'
};

class Homepage extends Component {
  render() {
    
    return (

        <div>
            <div className="container">
                <div className="fs-c-third one-r">
                    <h3 className="subtitle">Real Time Big Data Processing Project</h3>
                </div>
                <div className="t-c-third one-r">
                    <h4>11/09/2019</h4>
                </div>
                <div className="t-c-third">
                    <h4>Alessandro Dal Gobbo</h4>
                </div>
                        
            </div>
            <div className="container">
                <div className="full-c-third m-l">
                    <h2 style={centerText}>Stock Quotes Prices Prediction</h2>
                </div>
            </div>

            <div className="container">
                <div className="full-c-third m-s">
                    <p>The project has been developed for the course of real time big data processing and does not actually predict the prices of stock quotes.
                         This is just a piece of the stack that, with much more hardware and many more workers behind, could actually become something nice.
                    </p>
                </div>
            </div>
        </div>
    );
  }
}


export default Homepage;
