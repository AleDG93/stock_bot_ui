import React, { Component } from 'react';
import results from '../../results.txt';

class Results extends Component {

    constructor(props){
        super(props);
        this.state = {
          result: ''
        }
      }

  getResult(){

    fetch(results)
    .then((r) => r.text())
    .then(text  => {
        var allResults = text.split('\n');        
        this.setState({
            result: allResults[0]
        })
        
    })
  }


  componentDidMount(){
    this.getResult();
    };


  render() {

    return (

        <div className="row container">
            <div className="full-c-third">
                <h3 className="subtitle"> Result:</h3>
            </div>
            <div className="full-c-third">
                {this.state.result}
            </div>
        </div>
    );
  }
}

export default Results;
