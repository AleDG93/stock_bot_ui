import React, { Component } from 'react';
import {
    Link
  } from 'react-router-dom';

class Header extends Component {
  render() {
    return (
        <header>
            <nav>
                <ul>
                    <li className="first">
                        <Link to="/"><i className="icon-home"></i>&nbsp;Home</Link>
                    </li>

                    <li className="second">
                        <Link to="/twitter-stats"><i className="icon-home"></i>&nbsp;Tweeter Stats</Link>
                    </li>
                    <li className="third">
                        <Link to="/stock-options"><i className="icon-home"></i>&nbsp;Stock Options</Link>
                    </li>
                    <li className="fourth">
                        <Link to="/results"><i className="icon-home"></i>&nbsp;Results</Link>
                    </li>
                </ul>
            </nav>
        </header>

    );
  }
}

export default Header;
