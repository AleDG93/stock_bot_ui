import React, { Component } from 'react';
import ScrollArea from 'react-scrollbar';
import twitterData from '../../tweets.txt';

class Tweets extends Component {

    constructor(props){
        super(props);
        this.state = {
          data: []
        }
      }
      
  getData(){

    var tweets;
    var tweetDivs = [];
    var divs = [];
    fetch(twitterData)
    .then((r) => r.text())
    .then(text  => {
        tweets = text;
    })
    .then(() => {
        tweetDivs = tweets.split("Struct{CreatedAt")        
        }
    ).then(() => {

        tweetDivs.forEach(element => {
            var cleanedTweet= element.substr(0, element.indexOf(',Source=<')); 
            cleanedTweet= cleanedTweet.substr(cleanedTweet.indexOf('Text=')+5, tweetDivs.length); 
            divs.push(cleanedTweet);
        });
       
    }).then(() => {
        this.setState({
            data: divs
        })
    })
  }

  componentDidMount(){
      this.getData();
  };



  render() {

    return (
        <ScrollArea
        speed={0.8}
        className="area"
        contentClassName="content"
        horizontal={false}
        >
        <div>
        <div className="container border-tb">
                <div className="full-c-third one-r border-tb">
                    <h3 className="subtitle">Some statistics</h3>
                </div>
                <div className="f-c-third m-l">
                    <h4>Total number of tweets:</h4>
                    <p>{this.state.data.length}</p>
                </div>
                
            </div>
        </div><div className="tweet-table">
        <table>
        <tr>
                    <th>sender</th>
                    <th>content</th>
                    
            </tr>
       {
        this.state.data.reverse().map((item, i) => (
         
                <tr >
                    <td>{item.substr(item.indexOf('RT @')+3, item.indexOf(':')-3)}</td>
                    <td>{item.substr(item.indexOf(':')+1, item.length)}</td>
                </tr>
            
        ))
        }</table></div>
      </ScrollArea> 
    );
  }
}

export default Tweets;
